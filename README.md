# Ansible steps for prepare EC2 instances #
These playbooks realizes a set of actions in EC2 instances:
    - Configure the system paramiters for SAP instances
    - Install the packages required for SAP
    - create users: daaadm and sapadm
    - create groups: sapsys and sapinst
    - create and mount local FS and EFS

## How to use ##
